vdr-plugin-satip (2.4.1-3) unstable; urgency=medium

  * rebuild against vdr 2.6.9 (closes: #1098402)
  * add myself to uploaders
  * change build-depends from obsolete pkg-config to pkgconf
  * update standards-version to 4.7.0

 -- Christoph Martin <chrism@debian.org>  Thu, 20 Feb 2025 16:44:18 +0100

vdr-plugin-satip (2.4.1-2) unstable; urgency=medium

  * Fix debian/watch
  * Build-depend on vdr 2.6.0 and update standards version to 4.6.0
  * Remove Frank Neumann from uploaders

 -- Tobias Grimm <etobi@debian.org>  Wed, 12 Jan 2022 21:05:26 +0100

vdr-plugin-satip (2.4.1-1) unstable; urgency=medium

  * Update debhleper-compat to 13
  * Build-depend on vdr >= 2.4.7
  * Project moved to GitHub
  * New upstream version 2.4.1

 -- Tobias Grimm <etobi@debian.org>  Wed, 15 Dec 2021 08:42:26 +0100

vdr-plugin-satip (2.4.0-4) unstable; urgency=medium

  * Standards-Version: 4.4.1

 -- Tobias Grimm <etobi@debian.org>  Fri, 01 Nov 2019 13:42:54 +0100

vdr-plugin-satip (2.4.0-3) unstable; urgency=medium

  [ Jelmer Vernooĳ ]
  * Use secure copyright file specification URI.
  * Trim trailing whitespace.

  [ Tobias Grimm ]
  * Standards-Version: 4.4.0
  * Made pkg-config configurable in Makefile to allow cross-builds

 -- Tobias Grimm <etobi@debian.org>  Mon, 29 Jul 2019 19:44:24 +0200

vdr-plugin-satip (2.4.0-2) unstable; urgency=medium

  * Build-depend on vdr-dev >= 2.4.1

 -- Tobias Grimm <etobi@debian.org>  Sat, 20 Jul 2019 05:35:06 +0200

vdr-plugin-satip (2.4.0-1) unstable; urgency=medium

  * New upstream version
  * Dropped gcc7-patch - fixed upstream

 -- Tobias Grimm <etobi@debian.org>  Wed, 30 May 2018 22:30:44 +0200

vdr-plugin-satip (2.3.0-3) unstable; urgency=medium

  * VCS moved to salsa.debian.org
  * Replaced priority extra with optional
  * Build-depend on vdr-dev (>= 2.4.0)
  * Standards-Version: 4.1.3

 -- Tobias Grimm <etobi@debian.org>  Sun, 15 Apr 2018 19:42:26 +0200

vdr-plugin-satip (2.3.0-2) unstable; urgency=medium

  * Fixed FTBS with glibc 2.26 (Closes: #887689)

 -- Tobias Grimm <etobi@debian.org>  Sat, 27 Jan 2018 21:45:54 +0100

vdr-plugin-satip (2.3.0-1) unstable; urgency=medium

  * New upstream release
  * Build-depend on vdr-dev 2.3.7

 -- Tobias Grimm <etobi@debian.org>  Sun, 25 Jun 2017 18:53:08 +0200

vdr-plugin-satip (2.2.4-1) unstable; urgency=medium

  * New upstream release
  * Dropped GCC 6 fix - fixed upstream
  * Standards-Version: 3.9.8

 -- Tobias Grimm <etobi@debian.org>  Tue, 03 Jan 2017 19:59:56 +0100

vdr-plugin-satip (2.2.3-2) unstable; urgency=medium

  * Fixed "FTBFS with GCC 6 (Closes: #811948)

 -- Tobias Grimm <etobi@debian.org>  Sat, 23 Jan 2016 18:47:02 +0100

vdr-plugin-satip (2.2.3-1) unstable; urgency=medium

  [ Frank Neumann ]
  * New upstream release

 -- Tobias Grimm <etobi@debian.org>  Wed, 06 Jan 2016 18:55:34 +0100

vdr-plugin-satip (2.2.0-4) unstable; urgency=medium

  * Fixed debian/copyright - the license actually is plain GPL-2
  * Added Vcs-Git/Vcs-Browser fields

 -- Tobias Grimm <etobi@debian.org>  Sun, 11 Oct 2015 17:58:34 +0200

vdr-plugin-satip (2.2.0-3) unstable; urgency=medium

  * Preparing upload (Closes: #756298)
  * Dropped -dbg package in favour of future automatic debug packages
  * Switched to machine readable copyright file

 -- Tobias Grimm <etobi@debian.org>  Sun, 27 Sep 2015 18:06:24 +0200

vdr-plugin-satip (2.2.0-2) experimental; urgency=medium

  * Now supporting the /etc/vdr/conf.d mechanism

 -- Tobias Grimm <etobi@debian.org>  Mon, 23 Mar 2015 23:14:01 +0100

vdr-plugin-satip (2.2.0-1) experimental; urgency=medium

  * Added debian/watch
  * New upstream release

 -- Tobias Grimm <etobi@debian.org>  Sun, 01 Mar 2015 20:00:35 +0100

vdr-plugin-satip (1.0.2-3) experimental; urgency=medium

  * Build-depend on vdr-dev (>= 2.2.0)

 -- Tobias Grimm <etobi@debian.org>  Thu, 19 Feb 2015 12:39:54 +0100

vdr-plugin-satip (1.0.2-2) experimental; urgency=medium

  * Migrated package from Ubuntu/ to Debian
  * Added Tobias Grimm to the uploaders

 -- Tobias Grimm <etobi@debian.org>  Sat, 14 Feb 2015 18:24:51 +0100

vdr-plugin-satip (1.0.2-1fnu0~trusty) trusty; urgency=high

  * add /etc/vdr/plugin/plugin.satip.conf for plugin parameters

 -- Frank Neumann <fnu@yavdr.org>  Mon, 09 Feb 2015 01:03:26 +0100

vdr-plugin-satip (1.0.2-0fnu0~trusty) trusty; urgency=high

  * new upstream version
  * Added configurable CI slots.
  * Fixed parsing of the setup values.
  * Added an option to disable sources via sources.conf.
  * Added a command-line option to disable all the SAT>IP server quirks.
  * Updated Spanish and Catalan translations (Thanks to Gabriel Bonich).
  * Updated German translations (Thanks to Frank Neumann).

 -- Frank Neumann <fnu@yavdr.org>  Sun, 18 Jan 2015 20:11:17 +0100

vdr-plugin-satip (1.0.1-0fnu0~trusty) trusty; urgency=high

  * new upstream version
  * Updated the command-line help and README.
  * Fixed the server teardown.
  * Removed the unnecessary config directory definition.
  * Added a fallback for older glibc libraries.
  * Improved pid selection performance.
  * Added support for Digital Devices CI extension.

 -- Frank Neumann <fnu@yavdr.org>  Sat, 10 Jan 2015 15:43:48 +0100

vdr-plugin-satip (1.0.0+git20150105-0fnu0~trusty) trusty; urgency=high

  * new upstream snapshots
  * Added a fallback for older glibc libraries.
  * Removed the unnecessary config directory definition.
  * Fixed the server teardown.
  * Updated the command-line help and README.

 -- Frank Neumann <fnu@yavdr.org>  Tue, 06 Jan 2015 02:16:46 +0100

vdr-plugin-satip (1.0.0-0fnu0~trusty) trusty; urgency=high

  * new upstream version
  * Fixed the cable only device detection.
  * Added support for blacklisted sources.
  * Fixed server reuse for active transponders.
  * Added a preliminary support for Fritz!WLAN Repeater DVB-C (Thanks to Christian Wick).
  * Added a preliminary support for Telestar Digibit R1 (Thanks to Dirk Wagner).
  * Added a new device status menu.
  * Added support for SAT>IP frontend selection via Radio ID.
  * Added command-line switches for manually defining used SAT>IP servers and setting used tracing mode.
  * Added new STAT and TRAC commands into the SVDRP interface.
  * Refactored the tuner implementation.
  * Updated against SAT>IP protocol specification version 1.2.1.
  * Refactored input thread to increase performance.
  * Added plenty of performance tweaks (Thanks to Stefan Schallenberg).
  * Fixed EIT scan (Thanks to Stefan Schallenberg).

 -- Frank Neumann <fnu@yavdr.org>  Thu, 25 Dec 2014 12:29:49 +0100

vdr-plugin-satip (0.3.3+git20141222-0fnu0~trusty) trusty; urgency=high

  * new github snapshot

 -- Frank Neumann <fnu@yavdr.org>  Tue, 23 Dec 2014 20:00:00 +0100

vdr-plugin-satip (0.3.3-0fnu0~trusty) trusty; urgency=high

  * new upstream version
  * Added a validity check for the session member
  * Added a session id quirk for Triax TSS 400

 -- Frank Neumann <fnu@yavdr.org>  Tue, 20 May 2014 22:33:46 +0200

vdr-plugin-satip (0.3.2-0fnu0~trusty) trusty; urgency=high

  * new upstream version
  * Fixed model detection and OctopusNet DVB-C model quirks
  * Added a session id quirk for GSSBOX

 -- Frank Neumann <fnu@yavdr.org>  Sat, 10 May 2014 15:46:42 +0200

vdr-plugin-satip (0.3.1-0fnu0~trusty) trusty; urgency=high

  * new upstream version
  * Fixed the device discovery

 -- Frank Neumann <fnu@yavdr.org>  Mon, 28 Apr 2014 17:23:34 +0200

vdr-plugin-satip (0.3.0-0fnu0~trusty) trusty; urgency=high

  * new upstream version
  * Tweaked the pid update mechanism

 -- Frank Neumann <fnu@yavdr.org>  Sun, 20 Apr 2014 22:49:17 +0200

vdr-plugin-satip (0.2.3-0fnu0~trusty) trusty; urgency=high

  * new upstream version
  * Added Spanish translation (Thanks to Gabriel Bonich)
  * Fixed parameters of the OPTIONS command
  * Added a device identication into the user agent string
  * Removed unnecessary PLAY commands and header callbacks

 -- Frank Neumann <fnu@yavdr.org>  Sat, 19 Apr 2014 16:47:20 +0200

vdr-plugin-satip (0.2.2-0fnu0~trusty) trusty; urgency=high

  * new upstream version
  * Fixed the default keepalive interval
  * Refactored the section filtering
  * Added Catalan translation (Thanks to Gabriel Bonich)

 -- Frank Neumann <fnu@yavdr.org>  Sat, 05 Apr 2014 13:05:47 +0200

vdr-plugin-satip (0.2.1-0fnu1~trusty) trusty; urgency=high

  * new upstream version
  * Changed implementation to report about RTP packet errors on 5 minutes interval only.
  * Added a check to write new sections only if there is no data in the read socket.
  * Fixed keepalive heartbeat again.
  * another try to get debug symbols stripped seperately
  * add decrease_keepalive.patch for test purpose by fnu

 -- Frank Neumann <fnu@yavdr.org>  Sun, 30 Mar 2014 18:18:25 +0200

vdr-plugin-satip (0.2.0-0fnu0~trusty) trusty; urgency=high

  * new upstream version
  * Added support for cDevice::Ready().
  * Fixed pid leaking while disabling section filters.
  * Fixed keepalive heartbeat.

 -- Frank Neumann <fnu@yavdr.org>  Sat, 29 Mar 2014 12:04:57 +0100

vdr-plugin-satip (0.1.1-0fnu1~trusty) trusty; urgency=high

  * new upstream version
  * Changed code to utilize a proper XML library.
  * Refactored the session code.
  * Fixed EIT scan functionality.
  * Updated for vdr-2.1.6.

 -- Frank Neumann <fnu@yavdr.org>  Sun, 16 Mar 2014 17:16:51 +0100

vdr-plugin-satip (0.1.0-0fnu0~trusty) trusty; urgency=high

  * new upstream version
  * changed behaviour,
    does now re-use existing DVB channel entries.
  * does support DVB-S/S2, DVB-T/T2 and DVB-C/C2
    with DigitalDevices Octopus Net.

 -- Frank Neumann <fnu@yavdr.org>  Mon, 10 Mar 2014 22:34:00 +0100

vdr-plugin-satip (0.0.1-0fnu0~trusty) trusty; urgency=high

  * initial release

 -- Frank Neumann <fnu@yavdr.org>  Sun, 02 Mar 2014 18:00:00 +0100
